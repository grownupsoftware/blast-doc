== Properties

=== Overview

[[properties]]
Properties are normally loaded from a JSON file on the class path.  The json file name by default is `blast-server.json`.  This can be over-ridden by supplying the System Property `blast-server-config`

=== Example JSON file.
[source,javascript]
----
{
    "event-bus-thread-pool-size": "/2", <1>
    "blasters-thread-pool-size": "+0",  <2>
    "endpoint": {                       <3>
        "name": "General",
        "port": 8081,                   <4>
        "backlog": 1000                 <5>
    },
    "socket-conditioning": {            <6>
        "input-buffer-size": "8k",      <7>
        "output-buffer-size": "8k",     <8>
        "keep-alive": "true",           <9>
        "reuse-address": true,          <10>
        "so-linger": 0,                 <11>
        "no-delay" : true               <12>
    }
}
----

<1> [[eventbus-property]] Number of threads to use with the event bus. Default `half the available cores`.
<2> [[blasters-property]] Number of blaster threads.  Default `number of available cores`.
<3> [[endpoint-property]] Endpoint configuration. __Optional__ Only required if using one of the Blast supplied Engines.
<4> The port to listen on for inbound client connections.  Default `8081`.
<5> The maximum queue length for incoming requests.  Default `1000`.
<6> [[socketcond-property]] Socket Conditioning. __Optional__ Condition the inbound connecting socket.
<7> Size of the input buffer.  Default `8k`
<8> Size of the output buffer.  Default `8k`
<9> Sets the Socket keep alive setting.  Default `true`
<10> Sets the socket reuse-address option.  Default `true`
<11> Sets the socket linger option.  Default `0`
<12> Sets the TCP_NO_DELAY option.  Default `true`


.Thread Pool size notation.

This value can either be a literal value, or a value based off the number available cores, using a simple arithmetic expression. For example -2, would be the number of available cores, minus 2, or \*2 would be the number of available cores multiplied by 2. Single +, -. /, * can be used.

.Buffer size notation.

This value can be a literal number, expressed as bytes, or can be postfixed with a mnemonic denoting unit of measure. For example 1.5G, would denote 1.5 Gigabytes. A single m (megabytes), k (kilobytes) or g (gigabytes) can be used.

.Time notation.

The time value can be a literal expressed in milliseconds, or can be postfixed with a mnemonic denoting the unit of measure.  For example, 2s would resolve to 2000 milliseconds. A single d (days), h (hours), m (minutes) or s (seconds) can be used. 

=== Programmatic properties.

Properties don't need to be loaded from a JSON file, that can be supplied programatically.
If the JSON file can't be loaded, the defaults will be applied.  Properties implement the interface 'BlastProperties', and these can be used with the BlastBuilder.

[source,java]
----
public interface BlastProperties {

    public int getEventBusThreadSize();

    public int getBlastersThreadSize();

    public Endpoint getEndpoint();

    public int getInputBufferSize();

    public int getOutputBufferSize();

    public boolean isKeepAlive();

    public boolean isReuseAddress();

    public boolean isNoDelay();
    
    public int getSoLinger();

}
----

