== Command Message Module
[[cmd]]

=== ID

`co.gusl.command.decoder`

=== Overview

This module allows for a RPC type messaging between client and server.  The module listens for inbound messages from the client <<ClientInboundMessageEvent,ClientInboundMessageEvent>> and routes the message to the correct `Command Handler`.  Command Handlers have to be registered before use and this is normally done during the configure method of the Module, but can be added at any time.

=== Installation

The command module can be installed at the Builder process, registered with the blast server or required by another module.

[source,java]
----
    CommandModule commandModule = new CommandModule();
    
    BlastServer server = Blast.(commandModule);
----


=== Command Registration

The easiest way to register a command is to use the utility method on the `BlastServer`.
[source,java]
----
 commandModule.registerCommand("echo",      <1> 
        EchoCommandEvent.class,             <2>
        EchoMessage.class,                  <3>
        new EchoCommandHandler());          <4>
----

<1> Name of the command.
<2> The name of the class to be posted on the event bus when a message of this type is received by a client.
<3> The POJO representation of the inbound message that must extend CommandMessage.
<4> Class of the '@OnEvent' method which will be triggered when a command of that name is received by a client.

=== Command Message
[source,java]
----
public class EchoMessage extends CommandMessage {

    private String message;

    public EchoMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
----

The command message can be as simple or complicated as you wish.  In the example above, the JSON representation of the inbound message would be,

[source,javascript]
----
{
    "cmd" : "echo",
    "message" : "Hello Blast!"
}
----

but the inbound messages could be a complex hierarchical structure, as long at its contains the `cmd` node.  For convenience a correlated message is also provided. 

=== Correlated Command Message
[source,java]
----
public class CorrelatedMessage extends CommandMessage {

    protected Long correlationId;

    public CorrelatedMessage() {
    }

    public Long getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(Long correlationId) {
        this.correlationId = correlationId;
    }
----

=== Command Event

When a inbound message from the client is received and parsed into a command message, the registered `CommandEvent` will be placed onto the event bus.

[source,java]
----
public class EchoCommandEvent extends BlastCommandEvent<EchoMessage> {

    public EchoCommandEvent() {
    }

}
----

=== Command Event Handler

The event handler is just normal event bus subscriber, but the event will contain the inbound Client and Message.

[source,java]
----
public class EchoCommandHandler {

    private static final BlastLogger logger = BlastLogger.createLogger();

    public EchoCommandHandler() {
    }

    @OnEvent
    public void handleEcho(EchoCommandEvent event) {
        logger.info("Echo Received From {}", event.getClient().getAddressAndID());
        // It is possible that the message field is missing from the command data
        String message = event.getCommandData().getMessage();
        if (message != null) {
            // Send the message back to the client, via queues and blasters
            event.getClient().queueMessage(message.getBytes());
        } else {
            logger.info("Missing message field from Echo Message");
        }
    }
}
----

