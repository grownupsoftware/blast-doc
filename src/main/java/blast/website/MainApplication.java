package blast.website;

import blast.log.BlastLogger;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

/**
 *
 * @author grant
 */
public class MainApplication {

    private static final BlastLogger logger = BlastLogger.createLogger();
    
    private void start(int port) {
        Vertx vertx = Vertx.vertx(new VertxOptions().setWorkerPoolSize(5));

        // this will not work as resources bundled as a jar
        // System.setProperty("vertx.disableFileCaching", "true");
        Router router = Router.router(vertx);
        
        StaticHandler staticHandler = StaticHandler.create("webapp");
        staticHandler.setIndexPage("index.html");
        staticHandler.setCachingEnabled(false);
        
        router.route("/*").handler(staticHandler);
        
        logger.info("Website started on http://localhost:{}/", port);
        vertx.createHttpServer().requestHandler(router::accept).listen(port);
    }
    
    public static void main(String... args) throws Exception {
        Options options = new Options();
        options.addOption("port", true, "port for the website");
        
        int port = 8100;
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);
        String portStr = commandLine.getOptionValue("port");
        if (portStr != null && !portStr.isEmpty()) {
            port = Integer.valueOf(portStr);
        }
        
        MainApplication application = new MainApplication();
        application.start(port);
    }
    
}
