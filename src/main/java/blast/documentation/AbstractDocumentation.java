package blast.documentation;

import blast.log.BlastLogger;
import java.io.File;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 *
 * @author grant
 */
public abstract class AbstractDocumentation {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private final String theOutputFile;
    private final Generator theGenerator;

    public AbstractDocumentation(String outputFile, Generator generator) {
        theOutputFile = outputFile;
        theGenerator = generator;
    }

    public void generate() {
        try {
            logger.info("Generating documentation");

            cleanFile(theOutputFile);

            theGenerator.generate(theOutputFile);

            File generatedFile = new File(theOutputFile);

            logger.info("generated documentation to : {} ", theOutputFile);

            assertThat("generated file exists", generatedFile.exists());

        } catch (Exception ex) {
            logger.error("Failed to generate documentation", ex);
        }
    }

    void cleanFile(String outputFile) {

        File file = new File(outputFile);
        if (file.exists()) {
            file.delete();
        }
    }
}
