package blast.documentation.generate;

/**
 *
 * @author grant
 */
public enum DocumentationType {

    PDF("pdf"),
    HTML("html5"),
    HTML_DECK("html5");

    private final String type;

    DocumentationType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }

}
