package blast.documentation.generate;

import blast.documentation.exceptions.DocException;
import blast.documentation.generate.extensions.EventMacro;
import blast.documentation.generate.extensions.FieldsExtension;
import blast.documentation.generate.extensions.NewDirListing;
import blast.documentation.generate.extensions.TreeMacro;
import blast.documentation.generate.extensions.YumlMacro;
import blast.log.BlastLogger;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import static org.asciidoctor.internal.JRubyAsciidoctor.create;
import static org.asciidoctor.OptionsBuilder.options;
import org.asciidoctor.Placement;
import org.asciidoctor.SafeMode;
import org.asciidoctor.extension.JavaExtensionRegistry;

/**
 *
 * @author grant
 */
public class DocumentationBuilder {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    private static final String DOC_SRC_FOLDER = "doc";

    private final String theOutputDir;

    public DocumentationBuilder(String outputDir) {
        theOutputDir = outputDir;
    }

    private void createDocumentation() {

        document("pdf_manual", DocumentationType.PDF);

        document("documentation", DocumentationType.HTML);
        document("contribute", DocumentationType.HTML);
        document("download", DocumentationType.HTML);
        document("developer_manual", DocumentationType.HTML);

                document("blog", DocumentationType.HTML);
                document("blog", DocumentationType.PDF);

        documentPresentations("/documentation/presentations", "presentations", DocumentationType.HTML_DECK);

        logger.info("Start webiste server ./gradle blast-website:site");
        File home = new File(theOutputDir + File.separator + "index.html");
        logger.info("or, open file://{}", home.getAbsoluteFile());
    }

    private void document(String folder, DocumentationType documentationType) {
        File destFolder = new File(theOutputDir + File.separator + folder);

        // note: parent adoc must match the name of the folder e.g. download equals download/download.adoc
        convertFile(DOC_SRC_FOLDER + File.separator + folder + File.separator + folder + ".adoc", "Blast", destFolder, documentationType);
    }

    private void convertFile(String srcFile, String title, File destFile, DocumentationType documentationType) {
        Asciidoctor asciidoctor = create();

        String rootPrefix = calcRootPrefix(theOutputDir, destFile);

        Attributes attributes = new Attributes();
        if (documentationType != DocumentationType.HTML_DECK) {
            attributes.setTableOfContents(true);
            attributes.setTableOfContents2(Placement.LEFT);
            attributes.setLinkCss(true);
        }
        attributes.setTitle(title);

        Map<String, Object> attributeMap = attributes.map();
        if (documentationType == DocumentationType.HTML_DECK) {
            attributeMap.put("deckjsdir", rootPrefix + "js/deck.js");
        }
        attributeMap.put("icons", "font");
        attributeMap.put("stylesdir", rootPrefix + "css");
        attributeMap.put("stylesheet", "blast-blue-asciidoctor.css");
        attributeMap.put("source-highlighter", "coderay");
        attributeMap.put("toc-title", "Blast Server");
        attributeMap.put("toclevels", "2");
        attributeMap.put(":docinfo:", "shared");

        Map<String, Object> options = options()
                .safe(SafeMode.UNSAFE)
                .toDir(destFile)
                .mkDirs(true)
                .attributes(attributes)
                .attributes(attributeMap)
                .option("backend", documentationType.getType())
                .asMap();

        if (documentationType == DocumentationType.HTML_DECK) {
            List<String> templateDirs = new ArrayList<>();
            templateDirs.add("./doc/templates/haml");
            options.put("template_dirs", templateDirs);
        }

        JavaExtensionRegistry extensionRegistry = asciidoctor.javaExtensionRegistry();

        extensionRegistry.preprocessor(ChangeAttributeValuePreprocessor.class);

        extensionRegistry.block("tree", TreeMacro.class);
        extensionRegistry.block("dir_listing", NewDirListing.class);
        extensionRegistry.block("yuml", YumlMacro.class);
        extensionRegistry.block("field_doc", FieldsExtension.class);
        extensionRegistry.block("event_doc", EventMacro.class);

        File aDoc = new File(srcFile);
        if (aDoc.exists()) {
            logger.info("-- converting: {}", aDoc.getName());
            asciidoctor.convertFile(aDoc, options);
        } else {
            logger.error("file does not exist: {}", aDoc.getAbsoluteFile());

        }

    }

    private void documentPresentations(String srcFolder, String destFolder, DocumentationType documentationType) {
        File file = new File(DOC_SRC_FOLDER + File.separator + srcFolder);
        String[] directories = file.list((File current, String name) -> new File(current, name).isDirectory());

        for (String directory : directories) {
            File dir = new File(DOC_SRC_FOLDER + File.separator + srcFolder + File.separator + directory);
            File destFile = new File(theOutputDir + File.separator + destFolder + File.separator + dir.getName());

            convertFile(dir + File.separator + "presentation.adoc", dir.getName(), destFile, documentationType);
        }
    }

    public static void main(String... args) throws DocException {

        Options options = new Options();
        options.addOption("output_dir", true, "Destination Directory");

        String outputDir;
        try {
            CommandLineParser parser = new DefaultParser();
            CommandLine commandLine = parser.parse(options, args);
            outputDir = commandLine.getOptionValue("output_dir");
            if (outputDir == null || outputDir.isEmpty()) {
                logger.error("No root project directory specified");
                throw new DocException("No root project directory specified");
            }

            logger.info("Output directory: {}", outputDir);
        } catch (ParseException ex) {
            throw new DocException("Error parsing options");
        }

        DocumentationBuilder builder = new DocumentationBuilder(outputDir);
        builder.createDocumentation();
    }

    /**
     * 3rd party js libraries are in the web sites root folder - need to
     * calculate ".." to get back to root folder
     *
     * @param outputDir
     * @param destFile
     * @return
     */
    private String calcRootPrefix(String outputDir, File destFile) {

        File outDir = new File(outputDir);
        String dest = destFile.getAbsolutePath().replace(outDir.getAbsolutePath(), "");

        StringTokenizer stOR = new StringTokenizer(dest, "/");

        StringBuilder builder = new StringBuilder();
        for (int x = 0; x < stOR.countTokens(); x++) {
            builder.append("../");
        }
        return builder.toString();
    }

}
