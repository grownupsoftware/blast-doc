package blast.documentation.generate;

import blast.log.BlastLogger;
import java.io.File;
import java.nio.file.Paths;
import java.util.Map;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

/**
 *
 * @author grant
 */
public class ChangeAttributeValuePreprocessor extends Preprocessor {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    public ChangeAttributeValuePreprocessor(Map<String, Object> config) {
        super(config);
    }

    @Override
    public void process(Document document, PreprocessorReader reader) {
        logger.debug("document: {}", document.getAttributes());

        String workingDir = Paths.get("./doc").toAbsolutePath().normalize().toString();

        String sourceDir = Paths.get("src/main/java").toAbsolutePath().normalize().toString();
        String imageDir = Paths.get("src/main/resources/webapp/img").toAbsolutePath().normalize().toString();
        String imageHtmlDir = Paths.get("src/main/resources/webapp").toAbsolutePath().normalize().toString();

        //docdir=/Volumes/Striped/Users/grant/dev/gusl/master/blast-parent/blast-doc/doc/developer_manual        
        //docfile=/Volumes/Striped/Users/grant/dev/gusl/master/blast-parent/blast-doc/doc/developer_manual/developer_manual.adoc        
        String docDir = (String) document.getAttributes().get("docdir");
//        String docFile = (String) document.getAttributes().get("docfile");

        String sourceRelative = new File(docDir).toPath().relativize(new File(sourceDir).toPath()).toString();
        String imageRelative = new File(docDir).toPath().relativize(new File(imageDir).toPath()).toString();
        String imageHtmlRelative = new File(docDir).toPath().relativize(new File(workingDir).toPath()).toString();

        logger.debug("src rel: {}", sourceRelative);
        logger.debug("img rel: {}", imageRelative);
        logger.debug("img html rel: {}", imageHtmlRelative);

        String filetype = (String) document.getAttributes().get("filetype");
        if ("html".equals(filetype)) {
            // 'dirs' are relevant to server hone
            document.getAttributes().put("sourcedir", sourceRelative);
            document.getAttributes().put("imagesdir", imageHtmlRelative + "/img");
        }
        if ("pdf".equals(filetype)) {

            // created at run time so 'dirs' are relevant to code dirs
            document.getAttributes().put("sourcedir", sourceRelative);
            document.getAttributes().put("imagesdir", imageDir);
        }
    }

}
