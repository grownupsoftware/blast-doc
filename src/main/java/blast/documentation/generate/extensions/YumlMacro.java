package blast.documentation.generate.extensions;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

/**
 *
 * @author grant
 */
public class YumlMacro extends AbstractMacro {

    private static final String IMAGE_DIR = "src/main/resources/webapp/img/generated/";

    private static final String YUML_SERVER = "https://yuml.me/diagram/";

    private static int imageCounter = 1;

    public YumlMacro(String name, Map<String, Object> config) {
        super(name, config);
        turnOffSecurity();
    }

//    @Override
//    public Object process(AbstractBlock parent, Reader reader, Map<String, Object> attributes) {
//        Integer width = null;
//        Integer height = null;
//        String format = "scruffy";
//        String direction = null;
//        Integer scale = null;
//
//        logger.debug("yuml attributes: {}", attributes);
//        String type = (String) attributes.get("type");
//
//        String tmpStr = (String) attributes.get("width");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            width = Integer.valueOf(tmpStr);
//        }
//
//        tmpStr = (String) attributes.get("height");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            height = Integer.valueOf(tmpStr);
//        }
//
//        tmpStr = (String) attributes.get("format");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            format = tmpStr;
//        }
//
//        tmpStr = (String) attributes.get("scale");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            scale = Integer.valueOf(tmpStr);
//        }
//
//        tmpStr = (String) attributes.get("direction");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            direction = tmpStr;
//        }
//
////scale:180
//        //scruffy;dir:LR;scale:180
//        // so https://yuml.me/diagram/scruffy;dir:LR;scale:180/usecase/
//        //nofunky , plain, scruffy
//        StringBuilder builder = new StringBuilder();
//
////        builder.append(NL).append("****").append(NL);
//        builder.append("<div class=\"yuml\" >");
//        builder.append("<img ");
//        if (width != null) {
//            builder.append("width=\"").append(width).append("\" ");
//        }
//        if (height != null) {
//            builder.append("height=\"").append(width).append("\" ");
//        }
//        builder.append("src=\"");
//
//        builder.append(YUML_SERVER);
//        builder.append(format);
//        if (scale != null) {
//            builder.append(";").append("scale").append(":").append(scale);
//
//        }
//        if (direction != null) {
//            builder.append(";").append("dir").append(":").append(direction);
//
//        }
//        builder.append("/").append(type).append("/");
//
//        StringBuilder queryBuilder = new StringBuilder();
//        reader.readLines().stream().forEach((line) -> {
//            if (!line.isEmpty()) {
//                queryBuilder.append(line);//.replace("\n", ","));
//            }
//        });
//
//        builder.append(queryBuilder.toString());
//
////        try {
////            builder.append(URLEncoder.encode(queryBuilder.toString(), "UTF-8"));
////        } catch (UnsupportedEncodingException ex) {
////            logger.error("Failed to encode url", ex);
////        }
//        builder.append("\">");
//
//        builder.append("</div>");
//
//        logger.debug("---> {}", builder.toString());
//
//        return createBlock(parent, "pass", Arrays.asList(builder.toString()), attributes, this.getConfig());
//    }
    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
//        Integer width = null;
//        Integer height = null;
        String format = "scruffy";
//        String direction = null;
//        Integer scale = null;
        String tmpStr;

        logger.debug("yuml attributes: {}", attributes);

        StringBuilder urlBuilder = new StringBuilder();
        //urlBuilder.append(YUML_SERVER);

        tmpStr = (String) attributes.get("format");
        if (tmpStr != null && !tmpStr.isEmpty()) {
            urlBuilder.append(tmpStr);
        }

        tmpStr = (String) attributes.get("scale");
        if (tmpStr != null && !tmpStr.isEmpty()) {
            urlBuilder.append(";").append("scale").append(":").append(tmpStr);
        }

        tmpStr = (String) attributes.get("direction");
        if (tmpStr != null && !tmpStr.isEmpty()) {
            urlBuilder.append(";").append("dir").append(":").append(tmpStr);
        }

        tmpStr = (String) attributes.get("type");
        urlBuilder.append("/").append(tmpStr).append("/");

        StringBuilder queryBuilder = new StringBuilder();
        reader.readLines().stream().forEach((line) -> {
            if (!line.isEmpty()) {
                queryBuilder.append(line);//.replace("\n", ","));
            }
        });

        urlBuilder.append(queryBuilder.toString());

        //scale:180
        //scruffy;dir:LR;scale:180
        // so https://yuml.me/diagram/scruffy;dir:LR;scale:180/usecase/
        //nofunky , plain, scruffy
        String imageName;
        try {
            imageName = saveImage(urlBuilder.toString());
            List<Map<String, Object>> params = new ArrayList<>();
            Map<String, Object> paramValues = new HashMap<>();

            paramValues.put("target", "generated/" + "yuml_" + imageCounter + ".png");

//        String tmpStr = (String) attributes.get("width");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            urlBuilder.append("width=\"").append(tmpStr).append("\" ");
//
//        }
//
//        tmpStr = (String) attributes.get("height");
//        if (tmpStr != null && !tmpStr.isEmpty()) {
//            urlBuilder.append("height=\"").append(tmpStr).append("\" ");
//        }
            return createBlock(parent, "image", "", paramValues);
        } catch (Throwable ex) {
            logger.error("Failed to create yuml images", ex);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to create yuml image\n"}), attributes);
        }
    }

    private String saveImage(String urlString) throws MalformedURLException, IOException {

        URL url = new URL(YUML_SERVER + urlString);

        String imageName = IMAGE_DIR + "yuml_" + ++imageCounter + ".png";

        logger.info("yuml saving to: {} from url: {}", imageName, YUML_SERVER + URLEncoder.encode(urlString, "UTF-8"));

        OutputStream out;
        try (InputStream in = new BufferedInputStream(url.openStream())) {
            out = new BufferedOutputStream(new FileOutputStream(imageName));
            for (int i; (i = in.read()) != -1;) {
                out.write(i);
            }
        }
        out.close();

        return imageName;
    }

    /**
     * using https to go to yuml server - don't want cert validation
     */
    private void turnOffSecurity() {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }};

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }
    }
}
