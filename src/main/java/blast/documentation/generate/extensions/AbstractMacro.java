/*
 * Grownup Software Limited.
 */
package blast.documentation.generate.extensions;

import blast.log.BlastLogger;
import java.util.Arrays;
import java.util.Map;
import org.asciidoctor.extension.BlockProcessor;

/**
 *
 * @author dhudson - Apr 20, 2017 - 9:16:50 AM
 */
public abstract class AbstractMacro extends BlockProcessor {

    static final BlastLogger logger = BlastLogger.createLogger();
    static final String NL = "\n";

    public AbstractMacro(String name, Map<String, Object> config) {
        super(name, appendConfig(config));
    }

    private static Map<String, Object> appendConfig(Map<String, Object> config) {
        config.put("contexts", Arrays.asList(":listing"));
        return config;
    }

}
