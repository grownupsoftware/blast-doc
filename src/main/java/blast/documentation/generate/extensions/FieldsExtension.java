/*
 * Grownup Software Limited.
 */
package blast.documentation.generate.extensions;

import blast.doc.DocClass;
import static blast.documentation.BaseDocumentor.BOLD;
import static blast.documentation.BaseDocumentor.SPACE;
import static blast.documentation.BaseDocumentor.documentClass;
import static blast.documentation.generate.extensions.AbstractMacro.logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Reader;

/**
 *
 * @author grant
 */
public class FieldsExtension extends AbstractMacro {

    public FieldsExtension(String name, Map<String, Object> config) {
        super(name, config);
    }

    @Override
    public Object process(StructuralNode parent, Reader reader, Map<String, Object> attributes) {
        logger.debug("field attributes: {}", attributes);

        String className = (String) attributes.get("class");

        List<String> content = new ArrayList<>();
        try {
            Class<?> clazz = Class.forName(className);

            StringBuilder builder = new StringBuilder();

            builder.append(NL).append(BOLD).append("Class:").append(BOLD).append(SPACE)
                    .append(BOLD).append(clazz.getCanonicalName()).append(BOLD)
                    .append(NL);
            parseContent(parent, Arrays.asList(builder.toString().split("\n")));

            DocClass docClass = clazz.getAnnotation(DocClass.class);
            if (docClass != null) {
                builder = new StringBuilder();
                builder.append("Description: ").append(docClass.description()).append(NL);
                parseContent(parent, Arrays.asList(builder.toString().split("\n")));
            }

            StringBuilder aDoc = documentClass(clazz, false);
            aDoc.append(NL);

            content.addAll(Arrays.asList(aDoc.toString().split("\n")));
            parseContent(parent, content);

        } catch (ClassNotFoundException ex) {
            logger.error("Failed to find class: {}", className);
            return createBlock(parent, "pass", Arrays.asList(new String[]{"Failed to find class" + className + " - must be on class path\n"}), attributes);
        }

        return null;
    }

}
