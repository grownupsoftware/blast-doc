package blast.documentation;

import blast.doc.DocField;
import blast.json.ObjectMapperFactory;
import blast.log.BlastLogger;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author grant
 */
public class BaseDocumentor {

    protected static final BlastLogger logger = BlastLogger.createLogger();

    // asciidoctor uses equals for header number e.g. 1.1 or 1.1.1 , so we can modify the depth of the event header
    public static final String MAIN_HEADER = "= ";
    public static final String SECTION_HEADER = "== ";

    public static final String HEADER = "=== ";

    public static final String SUB_HEADER = "==== ";

    public static final String SUB_HEADER_2 = "===== ";

    public static final String SUB_HEADER_3 = "====== ";

    public static final String OPEN_TABLE = "[options=\"header\"]\n|===";
    public static final String OPEN_TABLE_NO_BORDERS_NO_HEADER = "[grid=\"none\",frame=\"none\"]\n|===";

    public static final String CLOSE_TABLE = "|===";

    public static final String BOLD = "*";

    public static final String SPACE = " ";

    public static final String SMALL_START = "[small]#";
    public static final String SMALL_END = "#";

    public static final String HORIZONTAL_LINE = "'''";

    public static final String COL = "|";
    public static final String COL_ASCII_DOC = "a|";
    public static final String HDR_COL = "|";

    public static final String START_BLOCK = "====";
    public static final String END_BLOCK = "====";
    public static final String BLOCK_HDR = ".";

    public static final String NUM_LIST = ". ";

    public static final String PAGE_BREAK = "<<<";

    public static final String LINE = "''' ";

    // new line
    public static final String NL = "\n";

    public static final String BEGIN_ANCHOR = "[[";
    public static final String END_ANCHOR = "]]";
    
    
    private final ObjectMapper theMapper = ObjectMapperFactory.createObjectMapper();

    public BaseDocumentor() {
        // replace NOT_NULL with always (our objects in doc are all blank)
        theMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);

    }

    /**
     * creates the ascii doctor link
     *
     * @param ref the reference (like href)
     * @param link what the user sees
     * @return the ascii doctor link
     */
    public String createLink(String ref, String link) {
        return "<<" + ref + "," + link + ">>";
    }

    /**
     * creates the ascii doctor anchor
     *
     * @param ref the reference (like href)
     * @return the ascii doctor link
     */
    public String createAnchor(String ref) {
        return "[[" + ref + "]]";
    }

    public static StringBuilder documentClass(Class theClass) {
        return documentClass(theClass, true);
    }

    /**
     * = Hello world
     * documents the event contents
     * 
     * {docdir} 
     * include::doc/documentation/tutorials/tutorial_2.adoc[]
     * 
     * [source,java]
     * ----
     *   foo bar
     * ----
     * @param theClass the event class
     * @param withLinks
     * @return ascii doctor string
     * [source,java]
     * ----
     */
    public static StringBuilder documentClass(Class theClass, boolean withLinks) {
        StringBuilder builder = new StringBuilder();

        List<Field> theFields = getFieldsFor(theClass);

        if (theFields.isEmpty()) {
            builder.append("Contains no information").append(NL).append(NL);
            return builder;
        }
        // generate contents table
        builder.append(OPEN_TABLE).append(NL);
        builder.append(HDR_COL).append("Name")
                .append(HDR_COL).append("Type")
                .append(HDR_COL).append("Description")
                .append(NL);

        theFields.stream().filter((theField) -> !(theField.getName().startsWith("$"))).filter((theField) -> !(theField.getName().equals("serialVersionUID"))).forEach((theField) -> {
            DocField annotation = theField.getAnnotation(DocField.class);
            String description = "Warning: Field has no documentation annotation (GsDoc)";
            if (annotation != null) {
                description = annotation.description();
            }

            String fieldType = formatField(theField.getType().getCanonicalName());

            if (theField.getType().getCanonicalName().endsWith("DO") || theField.getType().getCanonicalName().endsWith("DTO")) {
                // add a link if a DO
//                fieldType = "<<" + theField.getType().getCanonicalName() + "," + fieldType + ">>";
                if (withLinks) {
                    fieldType = "<<" + theField.getType().getCanonicalName() + "," + theField.getType().getSimpleName() + ">>";
                } else {
                    fieldType = theField.getType().getCanonicalName() + "," + theField.getType().getSimpleName();

                }

            }

            // add a link if a DO
            if (theField.getType().isEnum()) {
                Object[] enumConstants = theField.getType().getEnumConstants();
                StringBuilder enumBuilder = new StringBuilder();
                for (Object enumConstant : enumConstants) {
                    if (enumBuilder.length() > 0) {
                        enumBuilder.append(",");
                    }
                    enumBuilder.append(enumConstant);
                }
                description = "[" + enumBuilder.toString() + "]";
            }

            if ("List".equals(theField.getType().getSimpleName()) || "Set".equals(theField.getType().getSimpleName()) || "Map".equals(theField.getType().getSimpleName())) {

                ParameterizedType stringListType;

                try {
                    stringListType = (ParameterizedType) theField.getGenericType();

                    Class genericClass = (Class<?>) stringListType.getActualTypeArguments()[0];

                    fieldType = theField.getType().getSimpleName() + "<" + formatField(genericClass.getCanonicalName()) + ">";

                    // add a link if a DO
                    if (genericClass.isEnum()) {
                        Object[] enumConstants = genericClass.getEnumConstants();
                        StringBuilder enumBuilder = new StringBuilder();
                        for (Object enumConstant : enumConstants) {
                            if (enumBuilder.length() > 0) {
                                enumBuilder.append(",");
                            }
                            enumBuilder.append(enumConstant);
                        }
                        description = "[" + enumBuilder.toString() + "]";
                    }
                    if (genericClass.getCanonicalName().endsWith("DO") || genericClass.getCanonicalName().endsWith("DTO")) {
                        if (withLinks) {

                            fieldType = theField.getType().getSimpleName() + "<xref:" + genericClass.getCanonicalName() + "[" + formatField(genericClass.getSimpleName()) + "]>";
                        } else {
                            fieldType = theField.getType().getSimpleName();
                        }
                    }

                } catch (Exception e) {
                    logger.error("Failed to find ParameterizedType for {}", theClass.getSimpleName());
                }
            }

            builder.append(COL).append(theField.getName())
                    .append(COL).append(fieldType)
                    .append(COL).append(description)
                    .append(NL);
        });

        // close the contents table
        builder.append(CLOSE_TABLE).append(NL);

        return builder;
    }
    /*
    ----
    */

    /**
     * scans a class for all fields
     *
     * @param clazz the class to be scanned
     * @return the list of all fields
     */
    public static List<Field> getFieldsFor(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        fields.addAll(Arrays.asList(clazz.getDeclaredFields()));

        Class<?> parentClass = clazz.getSuperclass();

        while (parentClass != null) {
            fields.addAll(Arrays.asList(parentClass.getDeclaredFields()));
            parentClass = parentClass.getSuperclass();
        }

        return fields;
    }

    /**
     * Strip common packages from field type
     *
     * @param fieldName the field name
     * @return field name where common packages have been stripped
     */
    public static String formatField(String fieldName) {
        fieldName = fieldName.replace("java.lang.", "");
        fieldName = fieldName.replace("java.util.", "");
        fieldName = fieldName.replace("java.math.", "");
        return fieldName;
    }

    public StringBuilder addBlankExample(Class theClass) {
        StringBuilder theBuilder = new StringBuilder();
        try {

            //logger.info("Class: {}", theClass.getCanonicalName());
            Object theObject;

            // if you add a class here you should also add to ignoreClass in RestDocumentor
            switch (theClass.getCanonicalName()) {
                case "long":
                    theObject = new Long("0");
                    break;
                case "double":
                    theObject = new Double("0");
                    break;
                case "java.lang.Double":
                    theObject = new Double("0");
                    break;
                case "boolean":
                    theObject = Boolean.TRUE;
                    break;
                case "boolean[]":
                    theObject = new Boolean[0];
                    break;
                case "java.math.BigDecimal":
                    theObject = BigDecimal.ZERO;
                    break;
                case "java.lang.Boolean":
                    theObject = Boolean.TRUE;
                    break;
                case "java.lang.Integer":
                    theObject = new Integer("0");
                    break;
                case "java.util.List":
                    theObject = "[]";//new ArrayList<>();
                    break;
                case "java.lang.Long":
                    theObject = new Long("0");
                    break;
                case "java.lang.Long[]":
                    theObject = "[]";//new Long[0];
                    break;
                case "java.util.Map":
                    theObject = "{}";//new HashMap<>();
                    break;
                case "java.util.Set":
                    theObject = "{}";//new HashSet<>();
                    break;
                default:
                    if (theClass.isSynthetic()) {
                        theObject = "";
                    } else if (theClass.isEnum()) {
                        theObject = theClass.getEnumConstants();
                    } else {
                        try {
                            theObject = theClass.newInstance();
                        } catch (InstantiationException | IllegalAccessException ex) {
                            logger.error("Failed to create instance of {}", theClass.getCanonicalName());
                            theObject = "";
                        }
                    }
                    break;
            }
            String json = theMapper.writerWithDefaultPrettyPrinter().writeValueAsString(theObject);

            theBuilder.append(NL).append(BOLD).append("Example").append(BOLD).append(NL);

            theBuilder.append("[source,json]").append(NL);
            theBuilder.append("----").append(NL);
            json = json.replaceAll("null", "\"\"");

            theBuilder.append(json);
            theBuilder.append(NL).append("----").append(NL).append(NL);

        } catch (JsonProcessingException ex) {
            logger.error("Failed to create JSON example of object. Class: {} Error: {}", theClass.getCanonicalName(), ex.getMessage(), ex);
        }
        return theBuilder;
    }

}
