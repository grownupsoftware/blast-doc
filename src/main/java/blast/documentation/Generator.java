package blast.documentation;

/**
 *
 * @author grant
 */
public interface Generator {

    public void generate(String outputFile);
}
