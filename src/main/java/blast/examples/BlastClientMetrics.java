/*
 * Grownup Software Limited.
 */
package blast.examples;

import blast.Blast;
import blast.client.ClientClosedEvent;
import blast.client.ClientConnectingEvent;
import blast.eventbus.OnEvent;
import blast.exception.BlastException;
import blast.server.BlastServer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dhudson - Apr 20, 2017 - 10:29:46 AM
 */
public class BlastClientMetrics {

    private int connectedCount = 0;
    
    public BlastClientMetrics() {
    }

    @OnEvent
    public void onClientConnectingEvent(ClientConnectingEvent event, BlastServer server) {
        connectedCount++;
        if( connectedCount == 100) {
            server.postEvent(new ConnectedWatermarkEvent());
        }
    }

    @OnEvent
    public void onClientClosedEvent(ClientClosedEvent event) {
        connectedCount--;
    }

    public int getConnectedCount() {
        return connectedCount;
    }
    
    
    public static void install() {
        BlastServer blastServer = Blast.blast();
        BlastClientMetrics metrics = new BlastClientMetrics();
        blastServer.registerEventListener(metrics);
        try {
            blastServer.startup();
        } catch (BlastException ex) {
            
        }
    }
}
