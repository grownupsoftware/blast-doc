/*
 * Grownup Software Limited.
 */
package blast.examples;

import blast.client.BlastServerClient;
import blast.client.ClientConnectingEvent;
import blast.client.WebSocketRequestDetails;
import blast.eventbus.OnEvent;
import blast.exception.BlastException;

/**
 *
 * @author dhudson - Apr 20, 2017 - 11:27:51 AM
 */
public class BlastClientAuth {

    @OnEvent(order = 10, failFast = true, description = "Is the client allowed to connect?")
    public void authClient(ClientConnectingEvent event) throws BlastException {
        BlastServerClient client = event.getClient();
        WebSocketRequestDetails details = client.getRequestDetails();
        if (details.getHeaders().get("SiteToken") == null) {
            throw new BlastException("No Site Token Header");
        }
    }
}
